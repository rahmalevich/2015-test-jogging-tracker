//
//  Entry.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 12.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Entry : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * distance;
@property (nonatomic, retain) NSDate * time;
@property (nonatomic, retain) NSString * backend_id;
@property (nonatomic, retain) User *user;

@end
