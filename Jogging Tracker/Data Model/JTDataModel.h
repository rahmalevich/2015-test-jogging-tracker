//
//  JTDataModel.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "User.h"
#import "Entry.h"
#import "Entry+Extensions.h"
