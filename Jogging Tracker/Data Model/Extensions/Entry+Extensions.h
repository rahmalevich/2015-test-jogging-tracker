//
//  Entry+Extensions.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 14.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "Entry.h"

@interface Entry (Extensions)

- (CGFloat)averageSpeed;

@end
