//
//  Entry+Extensions.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 14.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "Entry+Extensions.h"

@implementation Entry (Extensions)

- (CGFloat)averageSpeed
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:self.time];
    CGFloat averageSpeed = ([self.distance floatValue] / 1000.0f) / ([components hour] + [components minute] / 60.0f + [components second] / 3600.0f);
    return averageSpeed;
}

@end
