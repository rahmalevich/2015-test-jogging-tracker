//
//  Entry.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 12.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "Entry.h"
#import "User.h"


@implementation Entry

@dynamic date;
@dynamic distance;
@dynamic time;
@dynamic backend_id;
@dynamic user;

@end
