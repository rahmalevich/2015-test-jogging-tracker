//
//  User.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 12.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "User.h"
#import "Entry.h"


@implementation User

@dynamic login;
@dynamic password;
@dynamic backend_id;
@dynamic entries;

@end
