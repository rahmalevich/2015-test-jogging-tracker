//
//  JTDataManager.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 12.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTDataManager.h"
#import "JTUserService.h"

#import <Parse/Parse.h>

@implementation JTDataManager

static JTDataManager *_sharedInstance = nil;
+ (JTDataManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [JTDataManager new];
    });
    return _sharedInstance;
}

- (Entry *)newEntryInContext:(NSManagedObjectContext *)context
{
    Entry *newEntry = [Entry MR_createInContext:context];
    newEntry.user = [User MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"SELF = %@", [JTUserService sharedInstance].authorizedUser] inContext:context];
    return newEntry;
}

- (void)updateEntriesWithCompletionHandler:(JTErrorBlock)completionHandler
{
    [completionHandler copy];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Entry"];
    [query whereKey:@"user" containedIn:@[[PFObject objectWithoutDataWithClassName:@"User" objectId:[JTUserService sharedInstance].authorizedUser.backend_id]]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if (!error) {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context){
                // Create and update entries
                NSMutableSet *receivedEntries = [NSMutableSet set];
                for (PFObject *object in objects) {
                    Entry *newEntry = [Entry MR_findFirstByAttribute:@"backend_id" withValue:object.objectId inContext:context];
                    if (!newEntry) {
                        newEntry = [Entry MR_createInContext:context];
                        newEntry.backend_id = object.objectId;
                        newEntry.user = [User MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"SELF = %@", [JTUserService sharedInstance].authorizedUser] inContext:context];
                    }
                    newEntry.date = object[@"date"];
                    newEntry.distance = object[@"distance"];
                    newEntry.time = object[@"time"];
                    [receivedEntries addObject:newEntry];
                }
                
                // Remove dropped entries from local database
                [Entry MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (SELF IN %@)", receivedEntries] inContext:context];
            } completion:^(BOOL success, NSError *error){
                if (completionHandler) {
                    completionHandler(error);
                }
            }];
        } else {
            if (completionHandler) {
                completionHandler(error);
            }
        }
    }];
}

- (void)saveEntry:(Entry *)entry completionHandler:(JTErrorBlock)completionHandler
{
    [completionHandler copy];
    
    if ([entry.backend_id isValidString]) {
        PFQuery *query = [PFQuery queryWithClassName:@"Entry"];
        [query getObjectInBackgroundWithId:entry.backend_id block:^(PFObject *pfEntry, NSError *error) {
            pfEntry[@"date"] = entry.date;
            pfEntry[@"distance"] = entry.distance;
            pfEntry[@"time"] = entry.time;
            [pfEntry saveInBackgroundWithBlock:^(BOOL succeed, NSError *error){
                if (succeed) {
                    [entry.managedObjectContext MR_saveToPersistentStoreAndWait];
                }
                if (completionHandler) {
                    completionHandler(error);
                }
            }];
        }];
    } else {
        PFObject *pfEntry = [PFObject objectWithClassName:@"Entry"];
        pfEntry[@"date"] = entry.date;
        pfEntry[@"distance"] = entry.distance;
        pfEntry[@"time"] = entry.time;
        pfEntry[@"user"] = [PFObject objectWithoutDataWithClassName:@"User" objectId:entry.user.backend_id];
        [pfEntry saveInBackgroundWithBlock:^(BOOL succeed, NSError *error){
            if (succeed) {
                entry.backend_id = pfEntry.objectId;
                [entry.managedObjectContext MR_saveToPersistentStoreAndWait];
            }
            if (completionHandler) {
                completionHandler(error);
            }
        }];
    }
}

- (void)deleteEntry:(Entry *)entry completionHandler:(JTErrorBlock)completionHandler
{
    [completionHandler copy];
    
    PFObject *pfEntry = [PFObject objectWithoutDataWithClassName:@"Entry" objectId:entry.backend_id];
    [pfEntry deleteInBackgroundWithBlock:^(BOOL suceeded, NSError *error){
        if (suceeded) {
            [entry MR_deleteEntity];
            [entry.managedObjectContext MR_saveToPersistentStoreAndWait];
        }
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

@end
