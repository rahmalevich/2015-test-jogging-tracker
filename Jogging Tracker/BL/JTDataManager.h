//
//  JTDataManager.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 12.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface JTDataManager : NSObject

+ (JTDataManager *)sharedInstance;

- (Entry *)newEntryInContext:(NSManagedObjectContext *)context;
- (void)updateEntriesWithCompletionHandler:(JTErrorBlock)completionHandler;
- (void)saveEntry:(Entry *)entry completionHandler:(JTErrorBlock)completionHandler;
- (void)deleteEntry:(Entry *)entry completionHandler:(JTErrorBlock)completionHandler;

@end
