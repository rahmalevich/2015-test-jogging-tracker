//
//  JTUserService.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface JTUserService : NSObject

@property (nonatomic, strong, readonly) User *authorizedUser;
@property (nonatomic, assign) BOOL isNewUser;

+ (JTUserService *)sharedInstance;
- (void)loginWithEmail:(NSString *)email password:(NSString *)password completionHandler:(JTErrorBlock)completionHandler;
- (void)registerWithEmail:(NSString *)email password:(NSString *)password completionHandler:(JTErrorBlock)completionHandler;
- (void)logout;

@end
