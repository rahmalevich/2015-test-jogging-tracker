//
//  JTUserService.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTUserService.h"
#import "JTTransitionsManager.h"

#import <Parse/Parse.h>

static NSString *kAuthorizedUserId = @"kAuthorizedUserId";

@interface JTUserService ()
@property (nonatomic, strong, readwrite) User *authorizedUser;
@end

@implementation JTUserService

#pragma mark - Initialization
static JTUserService *_sharedInstance = nil;
+ (JTUserService *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [JTUserService new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:kAuthorizedUserId];
        if ([userId isValidString]) {
            User *authorizedUser = [User MR_findFirstByAttribute:@"backend_id" withValue:userId];
            if (authorizedUser) {
                self.authorizedUser = authorizedUser;
            }
        }
    }
    return self;
}

#pragma mark - Authorization methods
- (void)processAuthorization:(PFObject *)object
{
    User *user = [User MR_findFirstByAttribute:@"login" withValue:object.objectId];
    if (!user) {
        user = [User MR_createEntity];
        user.backend_id = object.objectId;
        user.login = object[@"login"];
        user.password = object[@"password"];
        [user.managedObjectContext MR_saveToPersistentStoreAndWait];
    }
    self.authorizedUser = user;
    
    [[NSUserDefaults standardUserDefaults] setObject:user.backend_id forKey:kAuthorizedUserId];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loginWithEmail:(NSString *)email password:(NSString *)password completionHandler:(JTErrorBlock)completionHandler;
{
    if (completionHandler) {
        [completionHandler copy];
        
        __weak typeof(self) wself = self;
        PFQuery *query = [PFQuery queryWithClassName:@"User" predicate:[NSPredicate predicateWithFormat:@"login = %@ AND password = %@", email, password]];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error){
            if (object) {
                wself.isNewUser = NO;
                [wself processAuthorization:object];
                [[JTTransitionsManager sharedInstance] processAuthorization];
            }
            completionHandler(error);
        }];
    }
}

- (void)registerWithEmail:(NSString *)email password:(NSString *)password completionHandler:(JTErrorBlock)completionHandler;
{
    if (completionHandler) {
        [completionHandler copy];
        
        __weak typeof(self) wself = self;
        PFQuery *query = [PFQuery queryWithClassName:@"User" predicate:[NSPredicate predicateWithFormat:@"login = %@ AND password = %@", email, password]];
        [query countObjectsInBackgroundWithBlock:^(int number, NSError *error){
            if (!error) {
                if (number > 0) {
                    NSError *error = [NSError errorWithDomain:kJTErrorDomain code:JTErrorCodeUserAlreadyExists userInfo:nil];
                    completionHandler(error);
                } else {
                    PFObject *newUser = [PFObject objectWithClassName:@"User"];
                    newUser[@"login"] = email;
                    newUser[@"password"] = password;
                    [newUser saveInBackgroundWithBlock:^(BOOL suceed, NSError *error){
                        wself.isNewUser = YES;
                        [wself processAuthorization:newUser];
                        [[JTTransitionsManager sharedInstance] processAuthorization];
                    }];
                }
            } else {
                completionHandler(error);
            }
        }];
    }
}

- (void)logout
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kAuthorizedUserId];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[JTTransitionsManager sharedInstance] processLogout];
}

@end
