//
//  main.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JTAppDelegate class]));
    }
}
