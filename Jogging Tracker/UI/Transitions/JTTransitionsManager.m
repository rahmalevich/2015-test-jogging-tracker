//
//  JTTransitionsManager.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTTransitionsManager.h"

@implementation JTTransitionsManager

#pragma mark - Initialization
static JTTransitionsManager *_sharedInstance = nil;
+ (JTTransitionsManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [JTTransitionsManager new];
    });
    return _sharedInstance;
}

#pragma mark - Entry points
- (UIViewController *)authorizationEntryPointVC
{
    return [self viewControllerWithIdentifier:@"loginViewController"];
}

- (UIViewController *)appEntryPointVC
{
    return [self viewControllerWithIdentifier:@"entriesViewController"];
}

#pragma mark - Transitions
- (void)processAuthorization
{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    
    UIViewController *tabVC = [self appEntryPointVC];
    UIViewController *authVC = keyWindow.rootViewController;
    [keyWindow insertSubview:tabVC.view belowSubview:authVC.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        authVC.view.top = authVC.view.frameHeight;
    } completion:^(BOOL finished){
        keyWindow.rootViewController = tabVC;
        [authVC.view removeFromSuperview];
    }];
}

- (void)processLogout
{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    
    UIViewController *tabVC = keyWindow.rootViewController;
    UIViewController *authVC = [self authorizationEntryPointVC];
    authVC.view.top = keyWindow.frameHeight;
    [keyWindow addSubview:authVC.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        authVC.view.top = 0.0f;
    } completion:^(BOOL finished){
        keyWindow.rootViewController = authVC;
        [tabVC.view removeFromSuperview];
    }];
}

#pragma mark - Internal
- (UIViewController *)viewControllerWithIdentifier:(NSString *)storyboardID
{
    UIViewController *resultVC = nil;
    if ([storyboardID isValidString]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        resultVC = [storyboard instantiateViewControllerWithIdentifier:storyboardID];;
    }
    return resultVC;
}

@end
