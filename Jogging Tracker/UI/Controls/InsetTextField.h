//
//  InsetTextField.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 13.10.14.
//  Copyright (c) 2014 Mikhail Rakhmalevich. All rights reserved.
//

@interface InsetTextField : UITextField

@property (nonatomic, assign) UIEdgeInsets contentInset;

// overriden
- (CGRect)textRectForBounds:(CGRect)bounds;
- (CGRect)editingRectForBounds:(CGRect)bounds;

@end
