//
//  InsetTextField.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 13.10.14.
//  Copyright (c) 2014 Mikhail Rakhmalevich. All rights reserved.
//

#import "InsetTextField.h"

static CGFloat kDefaultEdgeInset = 10.0f;

@implementation InsetTextField

#pragma mark - Initialization
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.contentInset = UIEdgeInsetsMake(0.0f, kDefaultEdgeInset, 0.0f, kDefaultEdgeInset);
}

#pragma mark - Overriden methods
- (CGRect)textRectForBounds:(CGRect)bounds
{
    return [self contentRectForBounds:bounds];
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self contentRectForBounds:bounds];
}

- (CGRect)contentRectForBounds:(CGRect)bounds
{
    CGRect r1, r2;
    CGRectDivide(bounds, &r1, &r2, _contentInset.left, CGRectMinXEdge);
    CGRectDivide(r2, &r1, &r2, _contentInset.right, CGRectMaxXEdge);
    CGRectDivide(r2, &r1, &r2, _contentInset.top, CGRectMinYEdge);
    CGRectDivide(r2, &r1, &r2, _contentInset.bottom, CGRectMaxYEdge);
    return r2;
}

@end
