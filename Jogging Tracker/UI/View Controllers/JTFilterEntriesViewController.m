//
//  JTFilterEntriesViewController.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 14.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTFilterEntriesViewController.h"
#import "JTSettingsService.h"
#import "JTFormattingService.h"

#import "ActionSheetPicker.h"

@interface JTFilterEntriesViewController ()
@property (weak, nonatomic) IBOutlet UIButton *fromDateButton;
@property (weak, nonatomic) IBOutlet UIButton *toDateButton;
@end

@implementation JTFilterEntriesViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Filter";
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _fromDateButton.layer.borderWidth = 1.0f;
    _fromDateButton.layer.borderColor = [UIColor blackColor].CGColor;
    _toDateButton.layer.borderWidth = 1.0f;
    _toDateButton.layer.borderColor = [UIColor blackColor].CGColor;
    
    NSDate *fromDate = [JTSettingsService settingsValueForKey:kFilterFromDateKey];
    NSString *fromDateString = fromDate ? [[JTFormattingService dateTimeFormatter] stringFromDate:fromDate] : @"";
    [_fromDateButton setTitle:fromDateString forState:UIControlStateNormal];

    NSDate *toDate = [JTSettingsService settingsValueForKey:kFilterToDateKey];
    NSString *toDateString = toDate ? [[JTFormattingService dateTimeFormatter] stringFromDate:toDate] : @"";
    [_toDateButton setTitle:toDateString forState:UIControlStateNormal];
}

#pragma mark - Actions
- (IBAction)actionFromDate:(UIButton *)sender
{
    NSDate *fromDate = [JTSettingsService settingsValueForKey:kFilterFromDateKey];
    NSDate *toDate = [JTSettingsService settingsValueForKey:kFilterToDateKey];
    
    ActionSheetDatePicker *picker = [[ActionSheetDatePicker alloc] initWithTitle:@"From date" datePickerMode:UIDatePickerModeDateAndTime selectedDate:(fromDate ?: (toDate ?: [NSDate date])) minimumDate:nil maximumDate:(toDate ?: [NSDate date]) target:self action:@selector(fromDateWasSelected:element:) origin:sender];
    [picker showActionSheetPicker];
}

- (void)fromDateWasSelected:(NSDate *)date element:(id)element
{
    NSString *dateString = [[JTFormattingService dateTimeFormatter] stringFromDate:date];
    [_fromDateButton setTitle:dateString forState:UIControlStateNormal];

    [JTSettingsService setSettingsValue:date forKey:kFilterFromDateKey];
}

- (IBAction)actionToDate:(UIButton *)sender
{
    NSDate *fromDate = [JTSettingsService settingsValueForKey:kFilterFromDateKey];
    NSDate *toDate = [JTSettingsService settingsValueForKey:kFilterToDateKey];
    
    ActionSheetDatePicker *picker = [[ActionSheetDatePicker alloc] initWithTitle:@"To date" datePickerMode:UIDatePickerModeDateAndTime selectedDate:(toDate ?: [NSDate date]) minimumDate:(fromDate ?: nil) maximumDate:[NSDate date] target:self action:@selector(toDateWasSelected:element:) origin:sender];
    [picker showActionSheetPicker];
}

- (void)toDateWasSelected:(NSDate *)date element:(id)element
{
    NSString *dateString = [[JTFormattingService dateTimeFormatter] stringFromDate:date];
    [_toDateButton setTitle:dateString forState:UIControlStateNormal];
    
    [JTSettingsService setSettingsValue:date forKey:kFilterToDateKey];
}

- (IBAction)actionResetFilter:(UIButton *)sender
{
    [_fromDateButton setTitle:@"" forState:UIControlStateNormal];
    [_toDateButton setTitle:@"" forState:UIControlStateNormal];
    
    [JTSettingsService setSettingsValue:nil forKey:kFilterFromDateKey];
    [JTSettingsService setSettingsValue:nil forKey:kFilterToDateKey];
}

@end
