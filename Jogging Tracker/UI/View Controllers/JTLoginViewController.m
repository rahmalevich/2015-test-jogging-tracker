//
//  JTLoginViewController.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTLoginViewController.h"
#import "JTUserService.h"
#import "JTKeyboardHelper.h"

#import "MBProgressHUD.h"

@interface JTLoginViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (nonatomic, strong) JTKeyboardHelper *keyboardHelper;
@end

@implementation JTLoginViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.keyboardHelper = [[JTKeyboardHelper alloc] initWithScrollView:_scrollView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    _contentViewHeightConstraint.constant = self.view.frameHeight - [UIApplication sharedApplication].statusBarFrame.size.height;
}

#pragma mark - Utils
- (void)performLogin
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Authorization...";
    
    [[JTUserService sharedInstance] loginWithEmail:_loginTextField.text password:_passwordTextField.text completionHandler:^(NSError *error)
    {
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = error ? [JTStylesheet errorImageView] : [JTStylesheet successImageView];
        if (error) {
            hud.labelText = @"Error";
            hud.detailsLabelText = @"Invalid email or password";
        } else {
            hud.labelText = @"Authorization complete";
        }
        [hud hide:YES afterDelay:1.0];        
    }];
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:_loginTextField]) {
        [_passwordTextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self performLogin];
    }
    
    return YES;
}

#pragma mark - Actions
- (IBAction)actionLogin:(UIButton *)sender
{
    [self performLogin];
}

- (IBAction)actionRegister:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"registerSegue" sender:nil];
}

@end
