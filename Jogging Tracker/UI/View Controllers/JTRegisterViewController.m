//
//  JTRegisterViewController.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTRegisterViewController.h"
#import "JTUserService.h"
#import "JTKeyboardHelper.h"

#import "MBProgressHUD.h"

@interface JTRegisterViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFieldsCollection;

@property (nonatomic, strong) JTKeyboardHelper *keyboardHelper;
@end

@implementation JTRegisterViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _emailTextField.layer.borderWidth = 1.0f;
    _passwordTextField.layer.borderWidth = 1.0f;
    _confirmPasswordTextField.layer.borderWidth = 1.0f;
    
    self.keyboardHelper = [[JTKeyboardHelper alloc] initWithScrollView:_scrollView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    _contentViewHeightConstraint.constant = self.view.frameHeight - [UIApplication sharedApplication].statusBarFrame.size.height;    
}

#pragma mark - Utils
- (void)performRegistration
{
    if ([self validateFields]) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Registration...";
        
        [[JTUserService sharedInstance] registerWithEmail:_emailTextField.text password:_passwordTextField.text completionHandler:^(NSError *error)
        {
            hud.mode = MBProgressHUDModeCustomView;
            hud.customView = error ? [JTStylesheet errorImageView] : [JTStylesheet successImageView];
            if (error) {
                NSString *errorDescription = @"Please try again later";
                if ([error.domain isEqualToString:kJTErrorDomain]) {
                    if (error.code == JTErrorCodeUserAlreadyExists) {
                        errorDescription = @"User with such e-mail address is already registered";
                    }
                }
                hud.labelText = @"Error";
                hud.detailsLabelText = errorDescription;
            } else {
                hud.labelText = @"Registration complete";
            }
            [hud hide:YES afterDelay:1.0];
            
        }];
    }
}

- (BOOL)validateFields
{
    for (UITextField *textField in _textFieldsCollection) {
        textField.layer.borderColor = [UIColor clearColor].CGColor;
    }
    
    BOOL result = YES;
    
    if (![JTValidation validateEmail:_emailTextField.text]) {
        result = NO;
        _emailTextField.layer.borderColor = [UIColor redColor].CGColor;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter valid e-mail address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
    if (result && ![JTValidation validatePassword:_passwordTextField.text]) {
        result = NO;
        _passwordTextField.layer.borderColor = [UIColor redColor].CGColor;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your password should be at least 8 characters long and contain only letters and numbers" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
    if (result && ![_confirmPasswordTextField.text isEqualToString:_passwordTextField.text]) {
        result = NO;
        _confirmPasswordTextField.layer.borderColor = [UIColor redColor].CGColor;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Passwords doesn't match" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
    return result;
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:_emailTextField]) {
        [_passwordTextField becomeFirstResponder];
    } else if ([textField isEqual:_passwordTextField]) {
        [_confirmPasswordTextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self performRegistration];
    }

    return YES;
}

#pragma mark - Actions
- (IBAction)actionBack:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionRegister:(UIButton *)sender
{
    [self performRegistration];
}

@end
