//
//  JTEntriesTableCell.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 11.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTEntriesTableCell.h"

static CGFloat kEntriesTableCellHeight = 75.0f;

@interface JTEntriesTableCell ()
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end

@implementation JTEntriesTableCell

+ (CGFloat)cellHeight
{
    return kEntriesTableCellHeight;
}

@end
