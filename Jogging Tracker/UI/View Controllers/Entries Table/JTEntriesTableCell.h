//
//  JTEntriesTableCell.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 11.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTBaseTableCell.h"

@interface JTEntriesTableCell : JTBaseTableCell

@property (weak, nonatomic, readonly) UILabel *dateLabel;
@property (weak, nonatomic, readonly) UILabel *distanceLabel;
@property (weak, nonatomic, readonly) UILabel *timeLabel;

+ (CGFloat)cellHeight;

@end
