//
//  JTEntriesViewController.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTEntriesViewController.h"
#import "JTEntryDetailsViewController.h"
#import "JTEntriesTableHeaderView.h"
#import "JTBaseTableCell.h"
#import "JTEntriesTablePlaceholderCell.h"

#import "JTUserService.h"
#import "JTDataManager.h"
#import "JTFormattingService.h"
#import "JTSettingsService.h"

#import "MBProgressHUD.h"

static NSString *kEntrieCellReuseID = @"kEntrieCellReuseID";
static NSString *kPlaceholderCellReuseID = @"kPlaceholderCellReuseID";

#pragma mark - Empty data handler
@interface JTEntriesTableEmptyDataHandler : NSObject <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, copy) NSString *placeholderText;
@end

@implementation JTEntriesTableEmptyDataHandler

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [JTEntriesTablePlaceholderCell cellHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JTEntriesTablePlaceholderCell *cell = [tableView dequeueReusableCellWithIdentifier:kPlaceholderCellReuseID forIndexPath:indexPath];
    cell.placeholderLabel.text = _placeholderText ?: @"";
    return cell;
}

@end

#pragma mark - JTEntriesViewController
@interface JTEntriesViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) JTEntriesTableHeaderView *tableHeaderView;
@property (nonatomic, strong) UILabel *averageSpeedLabel;
@property (nonatomic, strong) UILabel *averageDistanceLabel;
@property (nonatomic, strong) NSFetchedResultsController *entriesController;
@property (nonatomic, strong) NSArray *filteredEntries;
@property (nonatomic, strong) JTEntriesTableEmptyDataHandler *emptyDataHandler;
@end

@implementation JTEntriesViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Entries";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Exit" style:UIBarButtonItemStylePlain target:self action:@selector(actionExit:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"+ Add" style:UIBarButtonItemStylePlain target:self action:@selector(actionAdd:)];
    
    self.tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"JTEntriesTableHeaderView" owner:self options:nil] lastObject];
    __weak typeof(self) wself = self;
    _tableHeaderView.filterBlock = ^{
        [wself performSegueWithIdentifier:@"filterSegue" sender:wself];
    };
    _tableHeaderView.reportBlock = ^{
        [wself performSegueWithIdentifier:@"reportSegue" sender:wself];
    };
    
    self.tableView.backgroundColor = [JTStylesheet commonBackgroundColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"JTBaseTableCell" bundle:nil] forCellReuseIdentifier:kEntrieCellReuseID];
    [self.tableView registerNib:[UINib nibWithNibName:@"JTEntriesTablePlaceholderCell" bundle:nil] forCellReuseIdentifier:kPlaceholderCellReuseID];
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(onTopRefresh:) forControlEvents:UIControlEventValueChanged];

    self.emptyDataHandler = [JTEntriesTableEmptyDataHandler new];
    self.entriesController = [Entry MR_fetchAllGroupedBy:nil withPredicate:[NSPredicate predicateWithFormat:@"user.backend_id = %@", [JTUserService sharedInstance].authorizedUser.backend_id] sortedBy:@"date" ascending:YES delegate:self];
    
    // Initial data updating
    if (![JTUserService sharedInstance].isNewUser) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[JTDataManager sharedInstance] updateEntriesWithCompletionHandler:^(NSError *error){
            [hud hide:YES];
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateAppearence];
}

#pragma mark - Appearence
- (void)updateAppearence
{
    NSMutableArray *predicatesArray = [NSMutableArray array];
    NSDate *fromDate = [JTSettingsService settingsValueForKey:kFilterFromDateKey];
    if (fromDate) {
        [predicatesArray addObject:[NSPredicate predicateWithFormat:@"date >= %@", fromDate]];
    }
    NSDate *toDate = [JTSettingsService settingsValueForKey:kFilterToDateKey];
    if (toDate) {
        [predicatesArray addObject:[NSPredicate predicateWithFormat:@"date <= %@", toDate]];
    }
    if ([predicatesArray count] > 0) {
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicatesArray];
        self.filteredEntries = [_entriesController.fetchedObjects filteredArrayUsingPredicate:predicate];
    } else {
        self.filteredEntries = _entriesController.fetchedObjects;
    }
    
    BOOL hasData = [_entriesController.fetchedObjects count] > 0;
    BOOL hasFilteredData = [_filteredEntries count] > 0;
    if (hasFilteredData) {
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.tableHeaderView = _tableHeaderView;
    } else {
        self.tableView.delegate = _emptyDataHandler;
        self.tableView.dataSource = _emptyDataHandler;
        self.tableView.tableHeaderView = hasData ? _tableHeaderView : nil;
        _emptyDataHandler.placeholderText = hasData ? @"No entries to show" : @"No entries";
    }
    
    [self.tableView reloadData];
}

- (void)configureCell:(JTBaseTableCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Entry *entry = _filteredEntries[indexPath.row];
    cell.titleLabel.text = [[JTFormattingService longDateTimeFormatter] stringFromDate:entry.date];
    cell.upperSubtitleLabel.text = [NSString stringWithFormat:@"Distance: %@ m   Time: %@", [entry.distance stringValue], [[JTFormattingService timeFormatter] stringFromDate:entry.time]];
    cell.lowerSubtitleLabel.text = [NSString stringWithFormat:@"Average speed: %.2f km/h", [entry averageSpeed]];
    
    JTTableCellSeparatorStyle cellStyle = JTTableCellSeparatorStyleDefault;
    if (indexPath.row == 0) {
        cellStyle |= JTTableCellSeparatorStyleTop;
    }
    if (indexPath.row == [_filteredEntries count] - 1) {
        cellStyle |= JTTableCellSeparatorStyleBottom;
    }
    cell.cellStyle = cellStyle;
}

#pragma mark - Updating data
- (void)onTopRefresh:(UIRefreshControl *)sender
{
    [[JTDataManager sharedInstance] updateEntriesWithCompletionHandler:^(NSError *error){
        [sender endRefreshing];
    }];
}

#pragma mark - Actions
- (void)actionExit:(UIBarButtonItem *)item
{
    [[JTUserService sharedInstance] logout];
}

- (void)actionAdd:(UIBarButtonItem *)item
{
    JTEntryDetailsViewController *detailsController = [[JTEntryDetailsViewController alloc] initWithEntry:nil];
    [self.navigationController pushViewController:detailsController animated:YES];
}

#pragma mark - UITableView delegate & datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [JTBaseTableCell cellHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_filteredEntries count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JTBaseTableCell *cell = [tableView dequeueReusableCellWithIdentifier:kEntrieCellReuseID forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Entry *entry = _filteredEntries[indexPath.row];
    JTEntryDetailsViewController *detailsController = [[JTEntryDetailsViewController alloc] initWithEntry:entry];
    [self.navigationController pushViewController:detailsController animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        Entry *entry = _filteredEntries[indexPath.row];
        [[JTDataManager sharedInstance] deleteEntry:entry completionHandler:^(NSError *error){
            hud.mode = MBProgressHUDModeCustomView;
            hud.customView = error ? [JTStylesheet errorImageView] : [JTStylesheet successImageView];
            [hud hide:YES afterDelay:1.0];
        }];
    }
}

#pragma mark - NSFetchedResultsController delegate
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self updateAppearence];
}

@end
