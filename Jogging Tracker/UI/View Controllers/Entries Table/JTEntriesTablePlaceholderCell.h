//
//  JTEntriesTablePlaceholderCell.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 13.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface JTEntriesTablePlaceholderCell : UITableViewCell

@property (weak, nonatomic, readonly) UILabel *placeholderLabel;

+ (CGFloat)cellHeight;

@end
