//
//  JTEntriesTableHeaderView.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 13.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface JTEntriesTableHeaderView : UIView

@property (nonatomic, copy) JTVoidBlock filterBlock;
@property (nonatomic, copy) JTVoidBlock reportBlock;

@end
