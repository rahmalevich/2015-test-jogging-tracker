//
//  JTEntriesTableHeaderView.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 13.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTEntriesTableHeaderView.h"

@interface JTEntriesTableHeaderView ()
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UIButton *reportButton;
@end

@implementation JTEntriesTableHeaderView

- (void)awakeFromNib
{
    [_filterButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor] size:_filterButton.size] forState:UIControlStateNormal];
    [_reportButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor] size:_reportButton.size] forState:UIControlStateNormal];
}

- (IBAction)actionFilter:(UIButton *)sender
{
    if (_filterBlock) {
        _filterBlock();
    }
}

- (IBAction)actionReport:(UIButton *)sender
{
    if (_reportBlock) {
        _reportBlock();
    }
}

@end
