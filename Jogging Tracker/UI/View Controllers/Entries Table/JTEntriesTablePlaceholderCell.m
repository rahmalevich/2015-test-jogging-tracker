//
//  JTEntriesTablePlaceholderCell.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 13.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTEntriesTablePlaceholderCell.h"

static CGFloat kPlaceholderCellHeight = 75.0f;

@interface JTEntriesTablePlaceholderCell ()
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@end

@implementation JTEntriesTablePlaceholderCell

+ (CGFloat)cellHeight
{
    return kPlaceholderCellHeight;
}

@end
