//
//  JTBaseTableCell.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 14.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

typedef NS_ENUM(NSUInteger, JTTableCellSeparatorStyle) {
    JTTableCellSeparatorStyleDefault = 0,
    JTTableCellSeparatorStyleTop = 1 << 0,
    JTTableCellSeparatorStyleBottom = 1 << 1,
};

@interface JTBaseTableCell : UITableViewCell

@property (weak, nonatomic, readonly) UIView *topSeparator;
@property (weak, nonatomic, readonly) UIView *bottomSeparator;
@property (weak, nonatomic, readonly) UILabel *titleLabel;
@property (weak, nonatomic, readonly) UILabel *upperSubtitleLabel;
@property (weak, nonatomic, readonly) UILabel *lowerSubtitleLabel;

@property (nonatomic, assign) JTTableCellSeparatorStyle cellStyle;

+ (CGFloat)cellHeight;

@end
