//
//  JTBaseTableCell.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 14.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTBaseTableCell.h"

static CGFloat kSeparatorOffset = 10.0f;
static CGFloat kEntriesTableCellHeight = 75.0f;

@interface JTBaseTableCell ()
@property (weak, nonatomic) IBOutlet UIView *topSeparator;
@property (weak, nonatomic) IBOutlet UIView *bottomSeparator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSeparatorTralingSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSeparatorLeadingSpaceConstraint;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *upperSubtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *lowerSubtitleLabel;
@end

@implementation JTBaseTableCell

+ (CGFloat)cellHeight
{
    return kEntriesTableCellHeight;
}

- (void)awakeFromNib
{
    self.cellStyle = JTTableCellSeparatorStyleDefault;
}

- (void)setCellStyle:(JTTableCellSeparatorStyle)cellStyle
{
    _cellStyle = cellStyle;
    
    _topSeparator.hidden = !(cellStyle & JTTableCellSeparatorStyleTop);
    
    CGFloat bottomSeparatorPadding = (cellStyle & JTTableCellSeparatorStyleBottom) ? 0.0f : kSeparatorOffset;
    _bottomSeparatorTralingSpaceConstraint.constant = bottomSeparatorPadding;
    _bottomSeparatorLeadingSpaceConstraint.constant = bottomSeparatorPadding;
}

@end
