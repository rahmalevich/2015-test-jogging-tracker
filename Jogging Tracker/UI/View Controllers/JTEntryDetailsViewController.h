//
//  JTEntryDetailsViewController.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface JTEntryDetailsViewController : UIViewController

- (instancetype)initWithEntry:(Entry *)entry;

@end
