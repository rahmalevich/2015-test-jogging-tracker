//
//  JTWeeksReportViewController.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 14.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTWeeksReportViewController.h"
#import "JTBaseTableCell.h"
#import "JTUserService.h"
#import "JTFormattingService.h"

static NSString * const kItemCellReuseID = @"kItemCellReuseID";

#pragma mark - JTWeeksReportItem
@interface JTWeeksReportItem : NSObject
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, assign) CGFloat averageSpeed;
@property (nonatomic, assign) CGFloat distance;
@end

@implementation JTWeeksReportItem
@end

#pragma mark - JTWeeksReportViewController
@interface JTWeeksReportViewController ()
@property (nonatomic, strong) NSArray *reportModel;
@end

@implementation JTWeeksReportViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Weeks report";
    
    self.tableView.backgroundColor = [JTStylesheet commonBackgroundColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"JTBaseTableCell" bundle:nil] forCellReuseIdentifier:kItemCellReuseID];
    
    [self setupModel];
}

- (void)setupModel
{
    NSMutableArray *entries = [[Entry MR_findAllSortedBy:@"date" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"user.backend_id = %@", [JTUserService sharedInstance].authorizedUser.backend_id]] mutableCopy];
    NSMutableArray *reportModel = [NSMutableArray array];
    
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    currentCalendar.firstWeekday = 2;
    
    NSDateComponents *oneWeek = [NSDateComponents new];
    oneWeek.weekOfMonth = -1;
    
    NSDate *startDate = [NSDate date], *endDate = [NSDate date];
    [currentCalendar rangeOfUnit:NSWeekCalendarUnit startDate:&startDate interval:NULL forDate:startDate];
    while ([entries count] > 0) {
        NSArray *weekEntries = [entries filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"date >= %@ AND date < %@", startDate, endDate]];
        
        CGFloat distance = 0;
        CGFloat averageSpeed = 0;
        if ([weekEntries count] > 0) {
            CGFloat totalSpeed = 0, totalMeters = 0;
            for (Entry *entry in weekEntries) {
                totalSpeed += [entry averageSpeed];
                totalMeters += [entry.distance floatValue];
            }
//            averageMeters = totalMeters / [weekEntries count];
            distance = totalMeters;
            averageSpeed = totalSpeed / [weekEntries count];
        }
        
        JTWeeksReportItem *newItem = [JTWeeksReportItem new];
        newItem.startDate = startDate;
        newItem.endDate = endDate;
        newItem.distance = distance;
        newItem.averageSpeed = averageSpeed;
        
        [reportModel insertObject:newItem atIndex:0];
        [entries removeObjectsInArray:weekEntries];
        
        endDate = startDate;
        startDate = [currentCalendar dateByAddingComponents:oneWeek toDate:startDate options:0];
    }
    
    self.reportModel = [NSArray arrayWithArray:reportModel];
}

#pragma mark - UITableView delegate & datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [JTBaseTableCell cellHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_reportModel count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JTBaseTableCell *cell = [tableView dequeueReusableCellWithIdentifier:kItemCellReuseID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
 
    JTWeeksReportItem *item = _reportModel[indexPath.row];
    NSDateFormatter *dateFormatter = [JTFormattingService dateFormatter];
    cell.titleLabel.text = [NSString stringWithFormat:@"%@ - %@", [dateFormatter stringFromDate:item.startDate], [dateFormatter stringFromDate:item.endDate]];
    cell.upperSubtitleLabel.text = [NSString stringWithFormat:@"Total distance: %.2f m", item.distance];
    cell.lowerSubtitleLabel.text = [NSString stringWithFormat:@"Average speed: %.2f km/h", item.averageSpeed];
    
    JTTableCellSeparatorStyle cellStyle = JTTableCellSeparatorStyleDefault;
    if (indexPath.row == 0) {
        cellStyle |= JTTableCellSeparatorStyleTop;
    }
    if (indexPath.row == [_reportModel count] - 1) {
        cellStyle |= JTTableCellSeparatorStyleBottom;
    }
    cell.cellStyle = cellStyle;
    
    return cell;
}

@end
