//
//  JTEntryDetailsViewController.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTEntryDetailsViewController.h"
#import "JTKeyboardHelper.h"
#import "JTFormattingService.h"
#import "JTDataManager.h"

#import "UIAlertView+Blocks.h"
#import "ActionSheetPicker.h"
#import "MBProgressHUD.h"

typedef NS_ENUM(NSUInteger, JTEntryDetailsMode) {
    JTEntryDetailsModeNew,
    JTEntryDetailsModeEdit
};

static NSInteger kTimeMaxHours = 10;

@interface JTEntryDetailsViewController () <ActionSheetCustomPickerDelegate, UITextFieldDelegate>
@property (nonatomic, assign) JTEntryDetailsMode mode;
@property (nonatomic, strong) NSManagedObjectContext *localContext;
@property (nonatomic, strong) Entry *entry;
@property (nonatomic, strong) JTKeyboardHelper *keyboardHelper;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *distanceTextField;
@property (weak, nonatomic) IBOutlet UIButton *dateButton;
@property (weak, nonatomic) IBOutlet UIButton *timeButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *actionButtonTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *averageSpeedLabel;
@end

@implementation JTEntryDetailsViewController

#pragma mark - Initialization
- (instancetype)initWithEntry:(Entry *)entry
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        if (entry) {
            self.mode = JTEntryDetailsModeEdit;
            self.entry = entry;
        } else {
            self.mode = JTEntryDetailsModeNew;
            self.localContext = [NSManagedObjectContext MR_context];
            self.entry = [[JTDataManager sharedInstance] newEntryInContext:_localContext];
        }
    }
    return self;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = _mode == JTEntryDetailsModeEdit ? @"Entry details" : @"Create entry";
    
    _distanceTextField.layer.borderWidth = 1.0f;
    _dateButton.layer.borderWidth = 1.0f;
    _dateButton.layer.borderColor = [UIColor blackColor].CGColor;
    _timeButton.layer.borderWidth = 1.0f;
    _timeButton.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.keyboardHelper = [[JTKeyboardHelper alloc] initWithScrollView:_scrollView];
    
    [self setupAppearence];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    _contentViewHeightConstraint.constant = self.view.frameHeight - [UIApplication sharedApplication].statusBarFrame.size.height - self.navigationController.navigationBar.size.height;
}

#pragma mark - Utils
- (void)setupAppearence
{
    if (_mode == JTEntryDetailsModeEdit) {
        [_dateButton setTitle:[[JTFormattingService dateTimeFormatter] stringFromDate:_entry.date] forState:UIControlStateNormal];
        [_timeButton setTitle:[[JTFormattingService timeFormatter] stringFromDate:_entry.time] forState:UIControlStateNormal];
        _distanceTextField.text = [_entry.distance stringValue];
        [self updateAverageSpeed];
    } else {
        [_actionButton setTitle:@"Create entry" forState:UIControlStateNormal];
        [_actionButton setTitleColor:[JTStylesheet commonOrangeColor] forState:UIControlStateNormal];
        _averageSpeedLabel.hidden = YES;
        _actionButtonTopConstraint.constant = 0.0f;
    }
}

- (void)updateAverageSpeed
{
    _averageSpeedLabel.text = [NSString stringWithFormat:@"Your average speed: %.2f km/h", [_entry averageSpeed]];
}

- (void)processEditing
{
    if (_mode == JTEntryDetailsModeEdit) {
        [[JTDataManager sharedInstance] saveEntry:_entry completionHandler:nil];
    }
}

- (BOOL)validateFields
{
    _distanceTextField.layer.borderColor = [UIColor clearColor].CGColor;
    _timeButton.layer.borderColor = [UIColor blackColor].CGColor;
    _dateButton.layer.borderColor = [UIColor blackColor].CGColor;
    
    BOOL result = YES;
    
    if (!_entry.date) {
        result = NO;
        _dateButton.layer.borderColor = [UIColor redColor].CGColor;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter date" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
    if (result && !_entry.distance) {
        result = NO;
        _distanceTextField.layer.borderColor = [UIColor redColor].CGColor;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter distance" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
    if (result && !_entry.time) {
        result = NO;
        _timeButton.layer.borderColor = [UIColor redColor].CGColor;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter time" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
    return result;
}
#pragma mark - Actions
- (IBAction)actionDate:(UIButton *)sender
{
    ActionSheetDatePicker *picker = [[ActionSheetDatePicker alloc] initWithTitle:@"" datePickerMode:UIDatePickerModeDateAndTime selectedDate:(_entry.date ?: [NSDate date]) minimumDate:nil maximumDate:[NSDate date] target:self action:@selector(dateWasSelected:element:) origin:sender];
    [picker addCustomButtonWithTitle:@"Today" value:[NSDate date]];
    [picker showActionSheetPicker];
}

- (void)dateWasSelected:(NSDate *)date element:(id)element
{
    NSString *dateString = [[JTFormattingService dateTimeFormatter] stringFromDate:date];
    [_dateButton setTitle:dateString forState:UIControlStateNormal];
    
    _entry.date = date;
    [self processEditing];
}

- (IBAction)actionTime:(UIButton *)sender
{
    AbstractActionSheetPicker *actionSheetPicker = [ActionSheetCustomPicker showPickerWithTitle:@"Select time" delegate:self showCancelButton:NO origin:sender initialSelections:nil];
    UIPickerView *pickerView = (UIPickerView *)actionSheetPicker.pickerView;

    if (_entry.time) {
        NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:_entry.time];
        [pickerView selectRow:[components hour] inComponent:0 animated:NO];
        [pickerView selectRow:[components minute] inComponent:1 animated:NO];
        [pickerView selectRow:[components second] inComponent:2 animated:NO];
    } else {
        [pickerView selectRow:1 inComponent:2 animated:NO];
    }
}

- (IBAction)actionButtonPressed:(UIButton *)sender
{
    if (_mode == JTEntryDetailsModeNew) {
        if ([self validateFields]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[JTDataManager sharedInstance] saveEntry:_entry completionHandler:^(NSError *error){
                hud.mode = MBProgressHUDModeCustomView;
                hud.customView = error ? [JTStylesheet errorImageView] : [JTStylesheet successImageView];
                [hud hide:YES afterDelay:1.0];
                if (!error) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    } else {
        RIButtonItem *noItem = [RIButtonItem itemWithLabel:@"No"];
        RIButtonItem *yesItem = [RIButtonItem itemWithLabel:@"Yes" action:^{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[JTDataManager sharedInstance] deleteEntry:_entry completionHandler:^(NSError *error){
                hud.mode = MBProgressHUDModeCustomView;
                hud.customView = error ? [JTStylesheet errorImageView] : [JTStylesheet successImageView];
                [hud hide:YES afterDelay:1.0];
                if (!error) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Do you really want to delete this entry?" cancelButtonItem:noItem otherButtonItems:yesItem, nil];
        [alertView show];
    }
}

#pragma mark - ActionSheetPicker delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger result;
    if (component == 0) {
        result = kTimeMaxHours;
    } else {
        result = 60;
    }
    return result;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%02ld", (long)row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSInteger hours = [pickerView selectedRowInComponent:0];
    NSInteger minutes = [pickerView selectedRowInComponent:1];
    NSInteger seconds = [pickerView selectedRowInComponent:2];
    if (hours == 0 && minutes == 0 & seconds == 0) {
        [pickerView selectRow:1 inComponent:2 animated:YES];
    }
}

- (void)actionSheetPickerDidSucceed:(AbstractActionSheetPicker *)actionSheetPicker origin:(id)origin
{
    UIPickerView *pickerView = (UIPickerView *)actionSheetPicker.pickerView;
    NSInteger hours = [pickerView selectedRowInComponent:0];
    NSInteger minutes = [pickerView selectedRowInComponent:1];
    NSInteger seconds = [pickerView selectedRowInComponent:2];
    NSString *timeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    [_timeButton setTitle:timeString forState:UIControlStateNormal];

    _entry.time = [[JTFormattingService timeFormatter] dateFromString:timeString];
    [self processEditing];
    
    [self updateAverageSpeed];
}

#pragma mark - UITextField delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:@"0"]) {
        textField.text = @"";
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text isValidString]) {
        _entry.distance = @([textField.text floatValue]);
    } else {
        _entry.distance = @(0);
        textField.text = @"0";
    }
    [self processEditing];
}

@end
