//
//  AppDelegate.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTAppDelegate.h"

#import "JTTransitionsManager.h"
#import "JTUserService.h"

#import <Parse/Parse.h>

@interface JTAppDelegate ()

@end

@implementation JTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [MagicalRecord setupAutoMigratingCoreDataStack];

    [Parse setApplicationId:kParseApplicationId clientKey:kParseClientKey];
    
    [JTStylesheet setupNavigationBarStyle];
    
    UIViewController *initialViewController = [JTUserService sharedInstance].authorizedUser ? [[JTTransitionsManager sharedInstance] appEntryPointVC] : [[JTTransitionsManager sharedInstance] authorizationEntryPointVC];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = initialViewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
