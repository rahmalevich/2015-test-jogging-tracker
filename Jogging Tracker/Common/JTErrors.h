//
//  JTErrors.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 27.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

static NSString *const kJTErrorDomain = @"kJTErrorDomain";
typedef NS_ENUM(NSUInteger, JTErrorCode) {
    JTErrorCodeUnknown = 0,
    JTErrorCodeUserAlreadyExists = 1
};