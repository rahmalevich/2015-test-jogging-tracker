//
//  JTStylesheet.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 11.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JTStylesheet : NSObject

+ (void)setupNavigationBarStyle;

// Fonts
+ (UIFont *)commonLabelsFontOfSize:(CGFloat)size;

// Colors
+ (UIColor *)commonOrangeColor;
+ (UIColor *)commonBackgroundColor;

// Images
+ (UIImageView *)successImageView;
+ (UIImageView *)errorImageView;

@end
