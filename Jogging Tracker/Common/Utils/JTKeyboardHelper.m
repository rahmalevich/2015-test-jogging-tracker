//
//  JTKeyboardHelper.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 14.10.14.
//  Copyright (c) 2014 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTKeyboardHelper.h"

@interface JTKeyboardHelper () <UIGestureRecognizerDelegate>
@property (nonatomic, strong, readwrite) UIScrollView *scrollView;
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, assign) CGFloat keyboardHeight;
@end

@implementation JTKeyboardHelper

#pragma mark - Initialization
- (instancetype)initWithScrollView:(UIScrollView *)scrollView
{
    if (self = [super init]) {
        self.scrollView = scrollView;

        self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapRecognized:)];
        _tapRecognizer.delegate = self;
        [_scrollView addGestureRecognizer:_tapRecognizer];

        // cancelTouchesInView = NO is made for processing button touches simulatenously
        // tap with gesture recognition
        _tapRecognizer.cancelsTouchesInView = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [_scrollView removeGestureRecognizer:_tapRecognizer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Tap recognizer
- (void)backgroundTapRecognized:(UITapGestureRecognizer *)tapRecognizer
{
    [[JTUIUtils findFirstResponderForView:_scrollView] resignFirstResponder];
}

#pragma mark - Keyboard notifications
- (void)keyboardWillChangeFrame:(NSNotification *)notification
{
    if (_scrollView.window) {
        NSDictionary *info = [notification userInfo];
        CGRect kbRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        CGFloat newKeyboardHeight = _scrollView.window.size.height - kbRect.origin.y;
        
        UIEdgeInsets contentInsets = _scrollView.contentInset;
        contentInsets.bottom += newKeyboardHeight - _keyboardHeight;
        _scrollView.contentInset = contentInsets;
        _scrollView.scrollIndicatorInsets = contentInsets;
        
        self.keyboardHeight = newKeyboardHeight;
    }
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    if (_scrollView.window) {
        NSDictionary *info = [notification userInfo];
        CGRect kbRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        
        CGRect aRect = _scrollView.frame;
        aRect.origin.y = _scrollView.contentOffset.y;
        aRect.size.height -= kbRect.size.height;
        
        UIView *firstResponder = [JTUIUtils findFirstResponderForView:_scrollView];
        CGPoint responderBottom = CGPointMake(firstResponder.x, firstResponder.y + firstResponder.frameHeight);
        CGPoint convertedBottom = [_scrollView convertPoint:responderBottom fromView:firstResponder.superview];
        if (!CGRectContainsPoint(aRect, convertedBottom))
        {
            [_scrollView scrollRectToVisible:[_scrollView convertRect:(CGRect){responderBottom, (CGSize){1, 1}} fromView:firstResponder.superview] animated:YES];
        }
    }
}

@end
