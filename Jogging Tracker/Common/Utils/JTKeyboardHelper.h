//
//  JTKeyboardHelper.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 14.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface JTKeyboardHelper : NSObject

@property (nonatomic, strong, readonly) UIScrollView *scrollView;

- (instancetype)initWithScrollView:(UIScrollView *)scrollView;

@end
