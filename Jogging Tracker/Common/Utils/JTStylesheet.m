//
//  JTStylesheet.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 11.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTStylesheet.h"

@implementation JTStylesheet

#pragma mark - Setup
+ (void)setupNavigationBarStyle
{
    NSDictionary *navBarAttributes = @{ NSFontAttributeName : [UIFont systemFontOfSize:17.0] };
    [[UINavigationBar appearance] setTitleTextAttributes:navBarAttributes];
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];

    NSDictionary *barButtonAttributes = @{  NSFontAttributeName : [UIFont systemFontOfSize:15.0],
                                            NSForegroundColorAttributeName : [UIColor blackColor] };
    [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonAttributes forState:UIControlStateNormal];
}

#pragma mark - Fonts
+ (UIFont *)commonLabelsFontOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:size];
}

#pragma mark - Colors
+ (UIColor *)commonOrangeColor
{
    return [UIColor colorWithHex:0xD96C00];
}

+ (UIColor *)commonBackgroundColor
{
    return [UIColor colorWithHex:0xE8ECEE];
}

#pragma mark - Images
+ (UIImageView *)successImageView
{
    UIImage *image = [UIImage imageNamed:@"icon_success"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    [imageView sizeToFit];
    
    return imageView;
}

+ (UIImageView *)errorImageView
{
    UIImage *image = [UIImage imageNamed:@"icon_error"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    [imageView sizeToFit];
    
    return imageView;
}

@end
