//
//  JTFormattingService.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 11.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface JTFormattingService : NSObject

+ (NSDateFormatter *)longDateTimeFormatter;
+ (NSDateFormatter *)dateTimeFormatter;
+ (NSDateFormatter *)dateFormatter;
+ (NSDateFormatter *)timeFormatter;

@end
