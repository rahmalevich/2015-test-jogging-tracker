//
//  JTUIUtils.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTUIUtils.h"

#pragma mark - UIUtils
@implementation JTUIUtils

+ (UIView *)findFirstResponderForView:(UIView *)aView
{
    UIView *resultView = nil;
    if ([aView isFirstResponder]) {
        resultView = aView;
    } else {
        for (UIView *subview in aView.subviews) {
            resultView = [self findFirstResponderForView:subview];
            if (resultView) {
                break;
            }
        }
    }
    return resultView;
}

@end

#pragma mark - UIColor convenience methods
@implementation UIColor (Convenience)

+ (UIColor *)colorWithHex:(int)hexValue
{
    return [self colorWithHex:hexValue alpha:1.];
}

+ (UIColor *)colorWithHex:(int)hexValue alpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0
                           green:((float)((hexValue & 0xFF00) >> 8))/255.0
                            blue:((float)(hexValue & 0xFF))/255.0
                           alpha:alpha];
}

@end

#pragma mark - UIImage convenience methods
@implementation UIImage (Convenience)

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, (CGRect){.size = size});
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end

#pragma mark - UIView convenience methods
@implementation UIView (Frames)

- (void)setOrigin:(CGPoint)loc
{
    CGRect rc = self.frame;
    rc.origin = loc;
    self.frame = rc;
}

- (void)setSize:(CGSize)sz
{
    CGRect rc = self.frame;
    rc.size = sz;
    self.frame = rc;
}

- (void)setX:(CGFloat)x
{
    CGRect rc = self.frame;
    rc.origin.x = x;
    self.frame = rc;
}

- (void)setY:(CGFloat)y
{
    CGRect rc = self.frame;
    rc.origin.y = y;
    self.frame = rc;
}

- (void)setLeft:(CGFloat)x
{
    self.x = x;
}

- (void)setTop:(CGFloat)y
{
    self.y = y;
}

- (void)setRight:(CGFloat)r
{
    CGRect rc = self.frame;
    rc.origin.x = r-rc.size.width;
    self.frame = rc;
}

- (void)setBottom:(CGFloat)b
{
    CGRect rc = self.frame;
    rc.origin.y = b-rc.size.height;
    self.frame = rc;
}

- (void)setFrameWidth:(CGFloat)w
{
    CGRect rc = self.frame;
    rc.size.width = w;
    self.frame = rc;
}

- (void)setFrameHeight:(CGFloat)h
{
    CGRect rc = self.frame;
    rc.size.height = h;
    self.frame = rc;
}

- (void)setCenterX:(CGFloat)x
{
    CGPoint pt = self.center;
    pt.x = x;
    self.center = pt;
}

- (void)setCenterY:(CGFloat)y
{
    CGPoint pt = self.center;
    pt.y = y;
    self.center = pt;
}

- (void)setBoundsX:(CGFloat)x
{
    CGRect rc = self.bounds;
    rc.origin.x = x;
    self.bounds = rc;
}

- (void)setBoundsY:(CGFloat)y
{
    CGRect rc = self.bounds;
    rc.origin.y = y;
    self.bounds = rc;
}

- (void)setBoundsWidth:(CGFloat)w
{
    CGRect rc = self.bounds;
    rc.size.width = w;
    self.bounds = rc;
}

- (void)setBoundsHeight:(CGFloat)h
{
    CGRect rc = self.bounds;
    rc.size.height = h;
    self.bounds = rc;
}

- (CGPoint)origin
{
    return self.frame.origin;
}

- (CGSize)size
{
    return self.frame.size;
}

- (CGFloat)x
{
    return self.frame.origin.x;
}

- (CGFloat)y
{
    return self.frame.origin.y;
}

- (CGFloat)top
{
    return self.frame.origin.y;
}

- (CGFloat)left
{
    return self.frame.origin.x;
}

- (CGFloat)right
{
    return self.frame.origin.x + self.frame.size.width;
}

- (CGFloat)bottom
{
    return self.frame.origin.y + self.frame.size.height;
}

- (CGFloat)frameWidth
{
    return self.frame.size.width;
}

- (CGFloat)frameHeight
{
    return self.frame.size.height;
}

- (CGFloat)centerX
{
    return self.center.x;
}

- (CGFloat)centerY
{
    return self.center.y;
}

- (CGFloat)boundsX
{
    return self.bounds.origin.x;
}

- (CGFloat)boundsY
{
    return self.bounds.origin.y;
}

- (CGFloat)boundsWidth
{
    return self.bounds.size.width;
}

- (CGFloat)boundsHeight
{
    return self.bounds.size.height;
}

@end