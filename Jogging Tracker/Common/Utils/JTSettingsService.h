//
//  JTSettingsService.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 14.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

NSString *kFilterFromDateKey;
NSString *kFilterToDateKey;

@interface JTSettingsService : NSObject

+ (id)settingsValueForKey:(NSString *)key;
+ (void)setSettingsValue:(id)value forKey:(NSString *)key;

@end
