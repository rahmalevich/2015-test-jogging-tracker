//
//  JTUIUtils.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 10.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface JTUIUtils : NSObject
+ (UIView *)findFirstResponderForView:(UIView *)aView;
@end

@interface UIColor (Convenience)
+ (UIColor *)colorWithHex:(int)hexValue;
+ (UIColor *)colorWithHex:(int)hexValue alpha:(CGFloat)alpha;
@end

@interface UIImage (Convenience)
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
@end

@interface UIView (Frames)
@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat left;
@property (nonatomic, assign) CGFloat top;
@property (nonatomic, assign) CGFloat bottom;
@property (nonatomic, assign) CGFloat right;
@property (nonatomic, assign) CGFloat frameHeight;
@property (nonatomic, assign) CGFloat frameWidth;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign) CGFloat boundsHeight;
@property (nonatomic, assign) CGFloat boundsWidth;
@property (nonatomic, assign) CGFloat boundsX;
@property (nonatomic, assign) CGFloat boundsY;
@end
