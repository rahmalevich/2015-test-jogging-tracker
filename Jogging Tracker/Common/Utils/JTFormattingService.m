//
//  JTFormattingService.m
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 11.01.15.
//  Copyright (c) 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "JTFormattingService.h"

static NSString * const kLongDateFormatterKey = @"kLongDateFormatterKey";
static NSString * const kDateTimeFormatterKey = @"kDateTimeFormatterKey";
static NSString * const kDateFormatterKey = @"kDateFormatterKey";

@interface JTFormattingService ()
+ (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format;
@end

@implementation JTFormattingService

#pragma mark - Caching
NSMutableDictionary *_sharedDictionary = nil;
+ (NSMutableDictionary *)sharedFormattersDicitionary
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedDictionary = [NSMutableDictionary dictionary];
    });
    return _sharedDictionary;
}

+ (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format
{
    NSDateFormatter *resultFormatter = [self sharedFormattersDicitionary][format];
    if (!resultFormatter) {
        resultFormatter = [[NSDateFormatter alloc] init];
        resultFormatter.dateFormat = format;
        [self sharedFormattersDicitionary][format] = resultFormatter;
    }
    return resultFormatter;
}

#pragma mark - Public
+ (NSDateFormatter *)longDateTimeFormatter
{
    NSDateFormatter *formatter = [self sharedFormattersDicitionary][kLongDateFormatterKey];
    if (!formatter) {
        formatter = [NSDateFormatter new];
        formatter.dateStyle = NSDateFormatterLongStyle;
        formatter.timeStyle = NSDateFormatterMediumStyle;
        [self sharedFormattersDicitionary][kLongDateFormatterKey] = formatter;
    }
    return formatter;
}

+ (NSDateFormatter *)dateTimeFormatter
{
    NSDateFormatter *formatter = [self sharedFormattersDicitionary][kDateTimeFormatterKey];
    if (!formatter) {
        formatter = [NSDateFormatter new];
        formatter.dateStyle = NSDateFormatterShortStyle;
        formatter.timeStyle = NSDateFormatterMediumStyle;
        [self sharedFormattersDicitionary][kDateTimeFormatterKey] = formatter;
    }
    return formatter;
}

+ (NSDateFormatter *)dateFormatter
{
    NSDateFormatter *formatter = [self sharedFormattersDicitionary][kDateFormatterKey];
    if (!formatter) {
        formatter = [NSDateFormatter new];
        formatter.dateStyle = NSDateFormatterShortStyle;
        formatter.timeStyle = NSDateFormatterNoStyle;
        [self sharedFormattersDicitionary][kDateFormatterKey] = formatter;
    }
    return formatter;
}

+ (NSDateFormatter *)timeFormatter
{
    NSDateFormatter *formatter = [self dateFormatterWithFormat:@"HH:mm:ss"];
    return formatter;
}

@end
