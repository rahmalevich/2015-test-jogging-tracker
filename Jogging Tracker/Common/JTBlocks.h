//
//  JTBlocks.h
//  Jogging Tracker
//
//  Created by Mikhail Rakhmalevich on 30.07.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

typedef void(^JTVoidBlock)(void);
typedef void(^JTDataBlock)(id data);
typedef void(^JTErrorBlock)(NSError *error);
